package org.launchcode.devops.mapnotesapi.NotesController;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.launchcode.devops.mapnotesapi.TestUtils.IntegrationTestConfig;

@IntegrationTestConfig
public class DeleteNoteTests {

  @Test
  @DisplayName("[empty state] DELETE /notes/{noteId}: 404 status")
  public void deleteNoteEmpty() throws Exception {
    assertTrue(false);
  }

  @Test
  @DisplayName("[populated state] DELETE /notes/{noteId}: 204 status")
  public void deleteNotePopulated() throws Exception {
    assertTrue(false);
  }
}
