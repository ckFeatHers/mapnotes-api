package org.launchcode.devops.mapnotesapi.NotesController;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.launchcode.devops.mapnotesapi.TestUtils.IntegrationTestConfig;

@IntegrationTestConfig
public class PostNotesTests {

  @Test
  @DisplayName("[valid data] POST /notes with body (NewNote JSON): 201 status and body (JSON Note entity)")
  public void createNoteValid() throws Exception {
    assertTrue(false);
  }

  @Test
  @DisplayName("[invalid data] POST /notes (NewNote JSON): 400 status and body (JSON field errors)")
  public void createNoteInvalid() throws Exception {
    assertTrue(false);
  }
}
