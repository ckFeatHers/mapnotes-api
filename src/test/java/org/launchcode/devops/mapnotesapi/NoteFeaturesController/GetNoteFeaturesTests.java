package org.launchcode.devops.mapnotesapi.NoteFeaturesController;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.launchcode.devops.mapnotesapi.TestUtils.IntegrationTestConfig;
import org.launchcode.devops.mapnotesapi.TestUtils.NoteFeatureEntityUtil;
import org.launchcode.devops.mapnotesapi.models.Feature.NoteFeatureEntity;

@IntegrationTestConfig
public class GetNoteFeaturesTests {

    @Test
    @DisplayName("[empty state] GET /notes/{noteId}/features: 404 status")
    public void getNonNoteFeatures() throws Exception {
        assertTrue(false);
    }

    @Test
    @DisplayName("[empty state] GET /notes/{noteId}/features: empty Feature Collection")
    public void getNoteFeaturesEmpty() throws Exception {
        assertTrue(false);        
    }

    @Test
    @DisplayName("[FeatureCollection] GET /notes/{noteId}/features: Feature Collection with 1 feature")
    public void getNoteFeaturesPopulated() throws Exception {
        // creates a pre-populated list of valid NoteFeatureEntities for testing purposes
        List<NoteFeatureEntity> noteFeatureEntities = NoteFeatureEntityUtil.getTestNoteFeatureEntities();
        
        assertTrue(false);
    }
}
