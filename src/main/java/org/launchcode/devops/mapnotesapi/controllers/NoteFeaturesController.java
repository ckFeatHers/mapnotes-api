package org.launchcode.devops.mapnotesapi.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;

// you will have to complete this class

// what path is this controller responsible for?
// what are each of the endpoints responsible for?

public class NoteFeaturesController {

    public ResponseEntity<Object> getNoteFeatures() {
        
        return ResponseEntity.status(418).build();
    }

    public ResponseEntity<Object> putNoteFeatures() {

        return ResponseEntity.status(418).build();
    }

}